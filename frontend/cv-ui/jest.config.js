// TODO replace with ts-jest/utils
const { pathsToModuleNameMapper } = require('./tools/jest.utils');
const { compilerOptions } = require('./tsconfig');

module.exports = {
  testMatch: ['<rootDir>/apps/cv/+(*.)+(spec|test).{ts,js}', '<rootDir>/libs/**/+(*.)+(spec|test).{ts,js}'],
  preset: 'jest-preset-angular',
  globals: {
    'ts-jest': {
      tsConfig: '<rootDir>/apps/cv/tsconfig.spec.json'
    }
  },
  collectCoverageFrom: ['<rootDir>/apps/cv/*.{ts,js}', '<rootDir>/libs/**/*.{ts,js}'],
  coverageReporters: ['text-summary'],
  moduleNameMapper: pathsToModuleNameMapper(compilerOptions.paths, '<rootDir>/'),
  coverageProvider: 'v8',
  setupFilesAfterEnv: ['<rootDir>/doubles/setupJest.ts'],
  transformIgnorePatterns: ['node_modules/(?!fp-ts)']
};
