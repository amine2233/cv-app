const { pathsToModuleNameMapper } = require('../../tools/jest.utils');
const { compilerOptions } = require('../../tsconfig.json');

module.exports = {
  rootDir: '../..',
  testMatch: ['<rootDir>/apps/cv-e2e/**/+(*.)+(spec|test).{ts,js}'],
  preset: 'jest-playwright-preset',
  testRunner: 'jest-circus/runner',
  testPathIgnorePatterns: ['/node_modules/'],
  transform: {
    '^.+\\.(ts|js|html)$': 'ts-jest'
  },
  coverageReporters: ['text-summary'],
  coverageProvider: 'v8',
  moduleNameMapper: pathsToModuleNameMapper(compilerOptions.paths, '<rootDir>/'),
  globals: {
    'ts-jest': {
      tsConfig: '<rootDir>/apps/cv-e2e/tsconfig.e2e.json'
    }
  },
  transformIgnorePatterns: ['/node_modules/(?!fp-ts)']
};
