module.exports = {
  browsers: ['chromium', 'firefox', 'webkit'],
  launchOptions: {
    args: ['--no-sandbox']
  }
};
