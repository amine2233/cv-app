import { from } from 'rxjs';
import { concatMap, switchMap, mapTo } from 'rxjs/operators';

describe('Initial page testing', () => {
  beforeEach(() => {});

  it('should be titled "CV UI"', async () => {
    await from(context.newPage())
      .pipe(
        concatMap(p => from(p.goto('http://localhost:4200')).pipe(mapTo(p))),
        switchMap(p => p.title())
        // tap(title => expect(title).toContain('CV UI'))
      )
      .toPromise();
  });
});
