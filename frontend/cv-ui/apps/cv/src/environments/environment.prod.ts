import { APPLICATION_BUILD_TIME, APPLICATION_BUILD_VERSION } from './version';

export const environment = {
  production: true,
  build: {
    version: APPLICATION_BUILD_VERSION,
    time: APPLICATION_BUILD_TIME
  }
};
