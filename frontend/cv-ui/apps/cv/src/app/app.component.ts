import { Component } from '@angular/core';

@Component({
  selector: 'my-root',
  template: ` <router-outlet></router-outlet> `,
  styles: []
})
export class AppComponent {}
