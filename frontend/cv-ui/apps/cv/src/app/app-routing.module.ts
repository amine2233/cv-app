import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuard } from '@cv/auth';
import { HomeLayoutComponent, LoginLayoutComponent, LoginComponent, RegisterComponent } from '@cv/ui';

const routes: Routes = [
  {
    path: '',
    component: HomeLayoutComponent,
    data: {
      links: [{ name: 'Documents', path: 'docs' }]
    },
    canActivate: [AuthGuard],
    children: [
      {
        path: 'docs',
        loadChildren: () => import('@cv/docs').then(m => m.DocsModule)
      },
      {
        path: '',
        redirectTo: '/docs',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    component: LoginLayoutComponent,
    children: [
      {
        path: 'login',
        component: LoginComponent
      },
      {
        path: 'register',
        component: RegisterComponent
      }
    ]
  },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule {}
