import { APP_INITIALIZER, Injectable, Provider } from '@angular/core';
import { ApplicationConfiguration, HostSettings, BuildSettings } from '@cv/core';
import { environment } from '../environments/environment';

export function configServiceFactory(config: ConfigService) {
  return () => config.loadConfig();
}

export function configHostSettingFactory(service: ConfigService) {
  return service.config.host;
}

export function buildSettingFactory(): BuildSettings {
  return new BuildSettings(environment.build.version, environment.build.time);
}

@Injectable()
export class ConfigService {
  public config: ApplicationConfiguration = new ApplicationConfiguration();

  public async loadConfig(): Promise<ApplicationConfiguration> {
    return new Promise((resolve, reject) => {
      // not use @angular/http to skip authorization
      const xhr = new XMLHttpRequest();
      xhr.open('GET', 'host.config.json', true);
      xhr.onload = () => {
        if (xhr.status >= 200 && xhr.status <= 301) {
          try {
            this.config = <any>JSON.parse(xhr.response);
          } catch (ex) {
            // TODO: add Maybe monad
          }
          resolve(this.config);
        } else {
          reject(xhr);
        }
      };
      xhr.send();
    });
  }
}

export const CONFIG_SERVICE_PROVIDERS: Provider[] = [
  ConfigService,
  {
    provide: APP_INITIALIZER,
    useFactory: configServiceFactory,
    deps: [ConfigService],
    multi: true
  },
  {
    provide: HostSettings,
    useFactory: configHostSettingFactory,
    deps: [ConfigService]
  },
  {
    provide: BuildSettings,
    useFactory: buildSettingFactory
  }
];
