import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import * as Sentry from '@sentry/angular';
import { Integrations } from '@sentry/tracing';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

if (environment.production) {
  enableProdMode();

  Sentry.init({
    dsn: 'https://0a65643a264245d883084cb24db18cda@o473632.ingest.sentry.io/5508698',
    integrations: [
      new Integrations.BrowserTracing({
        tracingOrigins: ['localhost', 'https://faircv.today', 'https://api.faircv.today'],
        routingInstrumentation: Sentry.routingInstrumentation
      })
    ],
    release: environment.build.version,
    tracesSampleRate: 1.0
  });
}

platformBrowserDynamic()
  .bootstrapModule(AppModule)
  .catch(err => console.error(err));
