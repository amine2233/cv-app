import { Component } from '@angular/core';

@Component({
  selector: 'my-input-file',
  templateUrl: './input-file.component.html',
  styleUrls: ['./input-file.component.css']
})
export class InputFileComponent {}
