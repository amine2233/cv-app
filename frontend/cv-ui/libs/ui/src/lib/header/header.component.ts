import { Component, Input } from '@angular/core';
import { NavBar } from '../shared/navbar.data';
import { HeaderComponentBloc } from './header.component.bloc';
import { AboutComponent } from '../about/about.component';

@Component({
  selector: 'my-ui-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {
  @Input() activeNavBars: NavBar[] = [];

  public about = AboutComponent;

  constructor(private bloc: HeaderComponentBloc) {}

  logout() {
    this.bloc.logout();
  }
}
