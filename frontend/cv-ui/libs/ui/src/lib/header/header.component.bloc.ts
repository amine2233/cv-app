import { Injectable } from '@angular/core';
import { AuthService } from '@cv/auth';

@Injectable()
export class HeaderComponentBloc {
  constructor(private auth: AuthService) {}

  public logout() {
    this.auth.logout();
  }
}
