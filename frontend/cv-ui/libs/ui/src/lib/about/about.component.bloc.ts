import { Injectable } from '@angular/core';
import { BuildSettings } from '@cv/core';
import { InfoService, log } from '@api/cv';
import { Observable } from 'rxjs';

@Injectable()
export class AboutComponentBloc {
  public info$: Observable<any>;
  constructor(private infoSvc: InfoService, public buildInfo: BuildSettings) {
    this.info$ = this.infoSvc.getInfos().pipe(log());
  }
}
