import { Component, OnInit } from '@angular/core';
import { BuildSettings } from '@cv/core';
import { AboutComponentBloc } from './about.component.bloc';
import { BaseComponent } from '../base.component';
import { InfoService } from '@api/cv';

@Component({
  selector: 'cv-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent extends BaseComponent implements OnInit {
  public info: any;
  public buildInfo: BuildSettings;

  constructor(private bloc: AboutComponentBloc, private testSvc: InfoService) {
    super();
    this.buildInfo = this.bloc.buildInfo;
  }

  ngOnInit() {
    this.addSubscription(this.bloc.info$.subscribe(i => (this.info = i)));
  }

  crash() {
    this.testSvc.throw().subscribe();
  }
}
