/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AboutComponent } from './about.component';
import { NgxFpTsModule } from 'ngx-fp-ts';
import { AboutComponentBloc } from './about.component.bloc';
import { InfoService } from '@api/cv';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BuildSettings } from '@cv/core';

describe('AboutComponent', () => {
  let component: AboutComponent;
  let fixture: ComponentFixture<AboutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [NgxFpTsModule, HttpClientTestingModule],
      providers: [
        AboutComponentBloc,
        InfoService,
        {
          provide: BuildSettings,
          useValue: <BuildSettings>{
            commit: 'test',
            time: 'now'
          }
        }
      ],
      declarations: [AboutComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AboutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
