import { Component } from '@angular/core';

@Component({
  selector: 'my-ui-login-layout',
  template: ` <router-outlet></router-outlet> `,
  styles: []
})
export class LoginLayoutComponent {}
