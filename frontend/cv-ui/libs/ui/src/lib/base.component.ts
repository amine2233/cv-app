import { Directive, OnDestroy } from '@angular/core';
import { Unsubscribable as AnonymousSubscription } from 'rxjs';
import { Utils } from '@cv/core';

@Directive()
export class BaseComponent implements OnDestroy {
  private subscriptions: { [key: string]: AnonymousSubscription } = {};

  protected addSubscription(subscription: AnonymousSubscription, key = '') {
    if (key === '') {
      key = Utils.createGuid();
    } else {
      this.removeSubscription(key);
    }
    this.subscriptions[key] = subscription;
  }

  protected removeSubscription(key: string) {
    if (this.subscriptions[key]) {
      this.subscriptions[key].unsubscribe();
      delete this.subscriptions[key];
    }
  }

  ngOnDestroy(): void {
    for (let key in this.subscriptions) {
      if (this.subscriptions[key]) {
        this.subscriptions[key].unsubscribe();
      }
    }
  }
}
