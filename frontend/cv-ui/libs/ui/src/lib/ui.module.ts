import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { HomeLayoutComponent } from './layouts/home-layout.component';
import { LoginLayoutComponent } from './layouts/login-layout.component';
import { HeaderComponent } from './header/header.component';
import { ReactiveFormsModule } from '@angular/forms';
import { UiMaterialModule } from './material.module';
import { RouterModule } from '@angular/router';
import { HelipopperModule } from '@ngneat/helipopper';
import { NgxFpTsModule } from 'ngx-fp-ts';
import { HeaderComponentBloc } from './header/header.component.bloc';
import { AboutComponentBloc } from './about/about.component.bloc';
import { AboutComponent } from './about/about.component';

const views = [
  LoginComponent,
  HomeLayoutComponent,
  LoginLayoutComponent,
  RegisterComponent,
  HeaderComponent,
  AboutComponent
];

const blocs = [HeaderComponentBloc, AboutComponentBloc];
@NgModule({
  declarations: [...views],
  imports: [CommonModule, RouterModule, UiMaterialModule, ReactiveFormsModule, HelipopperModule, NgxFpTsModule],
  exports: [...views],
  providers: [...blocs]
})
export class UiModule {}
