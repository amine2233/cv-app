export class HostSettings {
  api = '';
}

export class BuildSettings {
  constructor(public commit: string, public time: string) {}
}

export class ApplicationConfiguration {
  host: HostSettings = new HostSettings();
}
