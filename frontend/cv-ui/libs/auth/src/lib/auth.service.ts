import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Router } from '@angular/router';
import { HttpXhrBackend, HttpRequest, HttpResponse } from '@angular/common/http';
import { HostSettings } from '@cv/core';
import { UserData } from './user.data';
import { map, filter } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private tokenKey = 'CV__TOKEN';
  private tokenSubject: BehaviorSubject<string | undefined> = new BehaviorSubject<string | undefined>(undefined);

  get isLoggedIn$(): Observable<boolean> {
    return this.token$.pipe(map(token => (token ? true : false)));
  }

  get token$(): Observable<string | undefined> {
    return this.tokenSubject.asObservable();
  }

  constructor(
    private backend: HttpXhrBackend, // do not use HttpClient due to possible issues with interceptors
    private host: HostSettings,
    private router: Router
  ) {
    // just PoC, bad prqctice
    let token: string | null = localStorage.getItem(this.tokenKey);
    if (token) {
      this.tokenSubject.next(token);
    }
  }

  login(user: UserData) {
    let req: HttpRequest<any> = new HttpRequest(
      'PUT',
      this.host.api + '/api/users/login',
      {
        Name: user.Name,
        Password: user.Password
      },
      { responseType: 'text' }
    );
    this.backend
      .handle(req)
      .pipe(
        filter(e => e instanceof HttpResponse),
        map(e => e as HttpResponse<any>)
      )
      .subscribe((resp: HttpResponse<any>) => this.processResponse(resp));
  }

  logout() {
    localStorage.removeItem(this.tokenKey);
    this.tokenSubject.next(undefined);
    this.router.navigate(['/login']);
  }

  register(userData: UserData) {
    let req: HttpRequest<any> = new HttpRequest(
      'PUT',
      this.host.api + '/api/users',
      {
        Name: userData.Name,
        Password: userData.Password
      },
      { responseType: 'text' }
    );
    this.backend
      .handle(req)
      .pipe(
        filter(e => e instanceof HttpResponse),
        map(e => e as HttpResponse<any>)
      )
      .subscribe((resp: HttpResponse<any>) => this.processResponse(resp));
  }

  private processResponse(resp: HttpResponse<any>) {
    if (!resp.ok) return;
    let tokenValue = resp.body;
    localStorage.setItem(this.tokenKey, tokenValue);
    this.tokenSubject.next(tokenValue);
    this.router.navigate(['/']);
  }
}
