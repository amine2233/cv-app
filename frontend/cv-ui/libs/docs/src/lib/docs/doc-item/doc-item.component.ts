import { Component, Input, Output, EventEmitter } from '@angular/core';
import { DocumentModel } from '@api/cv';

@Component({
  selector: 'my-doc-item',
  templateUrl: './doc-item.component.html',
  styleUrls: ['./doc-item.component.scss']
})
export class DocItemComponent {
  @Input() doc!: DocumentModel;
  @Output() remove: EventEmitter<void> = new EventEmitter<void>();
  @Output() download: EventEmitter<void> = new EventEmitter<void>();

  public removeItem() {
    this.remove.emit();
  }

  public downloadItem() {
    this.download.emit();
  }
}
