import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { DocsListComponentBloc } from './docs-list.component.bloc';
import { ApiError, DocumentModel } from '@api/cv';
import { BaseComponent } from '@cv/ui';
import { Errors } from 'io-ts';
import { Either } from 'fp-ts/Either';

@Component({
  selector: 'my-docs-list',
  templateUrl: './docs-list.component.html',
  styleUrls: ['./docs-list.component.scss']
})
export class DocsListComponent extends BaseComponent {
  public loading$: Observable<boolean>;
  public data$: Observable<Either<ApiError, Either<Errors, DocumentModel>[]>>;

  constructor(private bloc: DocsListComponentBloc) {
    super();
    this.data$ = this.bloc.data$;
    this.loading$ = this.bloc.loading$;
  }

  upload(target: HTMLInputElement): void {
    const fileList = target.files;
    if (!!fileList && fileList.length > 0) {
      this.addSubscription(this.bloc.addDocument(fileList[0]).subscribe());
    }
  }

  removeDocument(doc: DocumentModel): void {
    this.addSubscription(this.bloc.removeDocument(doc.id).subscribe());
  }

  downloadDocument(doc: DocumentModel): void {
    this.addSubscription(this.bloc.downloadDocument(doc.id).subscribe());
  }
}
