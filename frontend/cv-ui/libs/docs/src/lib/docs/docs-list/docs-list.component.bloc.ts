import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { switchMap, filter, take, mapTo, tap } from 'rxjs/operators';
import { DocumentService, ApiError, DocumentModel } from '@api/cv';
import { Errors } from 'io-ts';
import { Either } from 'fp-ts/Either';

@Injectable()
export class DocsListComponentBloc {
  public data$: Observable<Either<ApiError, Either<Errors, DocumentModel>[]>>;
  public loading$: Observable<boolean>;

  private refreshRequired: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);
  private loadingSubj: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);

  public constructor(private svc: DocumentService) {
    this.loading$ = this.loadingSubj.asObservable();
    this.data$ = this.refreshRequired.pipe(
      filter(v => !!v),
      tap(() => this.loadingSubj.next(true)),
      switchMap(() => this.svc.getDocuments()),
      tap(() => this.loadingSubj.next(false))
    );
  }

  public addDocument(file: File): Observable<void> {
    return this.svc.saveDocument(file).pipe(take(1));
  }

  public removeDocument(id: string): Observable<void> {
    return this.svc.removeDocument(id);
  }

  public downloadDocument(id: string): Observable<void> {
    return this.svc.downloadDocument(id).pipe(mapTo(void 0)); // TODO: update to save file
  }
}
