import { async, TestBed } from '@angular/core/testing';
import { DocsModule } from './docs.module';

describe('DocsModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [DocsModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(DocsModule).toBeDefined();
  });
});
