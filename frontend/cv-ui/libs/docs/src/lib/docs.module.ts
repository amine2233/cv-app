import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxFpTsModule } from 'ngx-fp-ts';

import { DocsRoutingModule } from './docs-routing.module';
import { DocsListComponent } from './docs/docs-list/docs-list.component';
import { DocsComponent } from './docs/docs.component';
import { UiMaterialModule } from '@cv/ui';
import { DocItemComponent } from './docs/doc-item/doc-item.component';
import { DocsListComponentBloc } from './docs/docs-list/docs-list.component.bloc';

const blocs = [DocsListComponentBloc];
const localModules = [DocsRoutingModule, UiMaterialModule];
const externalModules = [CommonModule, NgxFpTsModule];
@NgModule({
  imports: [...localModules, ...externalModules],
  declarations: [DocsComponent, DocsListComponent, DocItemComponent],
  providers: [...blocs]
})
export class DocsModule {}
