import { Injectable } from '@angular/core';
import { tUsersService } from '../generated/services';
import { tInfoService } from '../generated/services/t-info.service';

@Injectable()
export class InfoService {
  constructor(private transportSvc: tInfoService, private user: tUsersService) {}

  public getInfos() {
    return this.transportSvc.infoGet$Json();
  }

  public throw() {
    return this.user.apiUsersThrowGet();
  }
}
