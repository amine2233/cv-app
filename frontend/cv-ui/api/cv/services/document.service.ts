import { Injectable } from '@angular/core';
import { map, mapTo } from 'rxjs/operators';
import { DocumentModel } from '../domain/models';
import { ApiError } from '../domain/errors';
import { none } from 'fp-ts/Option';
import { pipe } from 'fp-ts/function';
import { tDocumentsService } from '../generated/services/t-documents.service';
import { mapEither } from '../domain/functions';
import { mapLeft } from 'fp-ts/Either';

@Injectable()
export class DocumentService {
  constructor(private transportSvc: tDocumentsService) {}

  public getDocuments() {
    return this.transportSvc.apiDocumentsGet$Json().pipe(mapEither(tDocs => tDocs.map(d => DocumentModel.decode(d))));
  }

  public saveDocument(file: File) {
    return this.transportSvc
      .apiDocumentsPut({
        body: {
          files: [file]
        }
      })
      .pipe(mapTo(void 0));
  }

  public downloadDocument(documentId: string) {
    return this.transportSvc
      .apiDocumentsDocumentIdGet$Json({
        documentId: documentId
      })
      .pipe(
        map(transportItem =>
          pipe(
            DocumentModel.decode(transportItem),
            mapLeft(validation =>
              validation.reduce(
                (acc: ApiError, curr) => {
                  acc.userFriendlyError += curr.message;
                  return acc;
                },
                { userFriendlyError: '', details: none }
              )
            )
          )
        )
      );
  }

  public removeDocument(documentId: string) {
    return this.transportSvc
      .apiDocumentsDocumentIdDelete({
        documentId: documentId
      })
      .pipe(mapTo(void 0));
  }
}
