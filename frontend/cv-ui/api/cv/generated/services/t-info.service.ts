/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';
import { RequestBuilder } from '../request-builder';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';
import { eitherify } from '../../domain/functions';
import { ServerError } from '../../domain/errors';
import { Either } from 'fp-ts/Either';



@Injectable({
  providedIn: 'root',
})
export class tInfoService extends BaseService {
  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Path part for operation infoGet
   */
  static readonly InfoGetPath = '/Info';

  /**
   * Retrieves the information from application.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `infoGet$Plain()` instead.
   *
   * This method doesn't expect any request body.
   */
  infoGet$Plain$Response(params?: {

    /**
     * The requested API version
     */
    'api-version'?: string;

  }): Observable<StrictHttpResponse<{ [key: string]: {  } }>> {

    const rb = new RequestBuilder(this.rootUrl, tInfoService.InfoGetPath, 'get');
    if (params) {

      rb.query('api-version', params['api-version'], {});

    }
    return this.http.request(rb.build({
      responseType: 'text',
      accept: 'text/plain'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<{ [key: string]: {  } }>;
      })
    );
  }

  /**
   * Retrieves the information from application.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `infoGet$Plain$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  infoGet$Plain(params?: {

    /**
     * The requested API version
     */
    'api-version'?: string;

  }): Observable<Either<ServerError, { [key: string]: {  } }>> {

    return this.infoGet$Plain$Response(params).pipe(
      eitherify()
    );
  }
  /**
   * Retrieves the information from application.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `infoGet$Json()` instead.
   *
   * This method doesn't expect any request body.
   */
  infoGet$Json$Response(params?: {

    /**
     * The requested API version
     */
    'api-version'?: string;

  }): Observable<StrictHttpResponse<{ [key: string]: {  } }>> {

    const rb = new RequestBuilder(this.rootUrl, tInfoService.InfoGetPath, 'get');
    if (params) {

      rb.query('api-version', params['api-version'], {});

    }
    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'text/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<{ [key: string]: {  } }>;
      })
    );
  }

  /**
   * Retrieves the information from application.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `infoGet$Json$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  infoGet$Json(params?: {

    /**
     * The requested API version
     */
    'api-version'?: string;

  }): Observable<Either<ServerError, { [key: string]: {  } }>> {

    return this.infoGet$Json$Response(params).pipe(
      eitherify()
    );
  }
}