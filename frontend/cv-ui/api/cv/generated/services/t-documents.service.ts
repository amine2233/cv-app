/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';
import { RequestBuilder } from '../request-builder';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';
import { eitherify } from '../../domain/functions';
import { ServerError } from '../../domain/errors';
import { Either } from 'fp-ts/Either';


import { Document } from '../models/document';

@Injectable({
  providedIn: 'root',
})
export class tDocumentsService extends BaseService {
  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Path part for operation apiDocumentsGet
   */
  static readonly ApiDocumentsGetPath = '/api/Documents';

  /**
   * Retrieves the list of documents.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `apiDocumentsGet$Plain()` instead.
   *
   * This method doesn't expect any request body.
   */
  apiDocumentsGet$Plain$Response(params?: {

    /**
     * The requested API version
     */
    'api-version'?: string;

  }): Observable<StrictHttpResponse<Array<Document>>> {

    const rb = new RequestBuilder(this.rootUrl, tDocumentsService.ApiDocumentsGetPath, 'get');
    if (params) {

      rb.query('api-version', params['api-version'], {});

    }
    return this.http.request(rb.build({
      responseType: 'text',
      accept: 'text/plain'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<Array<Document>>;
      })
    );
  }

  /**
   * Retrieves the list of documents.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `apiDocumentsGet$Plain$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  apiDocumentsGet$Plain(params?: {

    /**
     * The requested API version
     */
    'api-version'?: string;

  }): Observable<Either<ServerError, Array<Document>>> {

    return this.apiDocumentsGet$Plain$Response(params).pipe(
      eitherify()
    );
  }
  /**
   * Retrieves the list of documents.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `apiDocumentsGet$Json()` instead.
   *
   * This method doesn't expect any request body.
   */
  apiDocumentsGet$Json$Response(params?: {

    /**
     * The requested API version
     */
    'api-version'?: string;

  }): Observable<StrictHttpResponse<Array<Document>>> {

    const rb = new RequestBuilder(this.rootUrl, tDocumentsService.ApiDocumentsGetPath, 'get');
    if (params) {

      rb.query('api-version', params['api-version'], {});

    }
    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'text/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<Array<Document>>;
      })
    );
  }

  /**
   * Retrieves the list of documents.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `apiDocumentsGet$Json$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  apiDocumentsGet$Json(params?: {

    /**
     * The requested API version
     */
    'api-version'?: string;

  }): Observable<Either<ServerError, Array<Document>>> {

    return this.apiDocumentsGet$Json$Response(params).pipe(
      eitherify()
    );
  }
  /**
   * Path part for operation apiDocumentsPut
   */
  static readonly ApiDocumentsPutPath = '/api/Documents';

  /**
   * Uploading new documents to the database.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `apiDocumentsPut()` instead.
   *
   * This method sends `multipart/form-data` and handles request body of type `multipart/form-data`.
   */
  apiDocumentsPut$Response(params?: {

    /**
     * The requested API version
     */
    'api-version'?: string;
      body?: { 'files': Array<Blob> }
  }): Observable<StrictHttpResponse<void>> {

    const rb = new RequestBuilder(this.rootUrl, tDocumentsService.ApiDocumentsPutPath, 'put');
    if (params) {

      rb.query('api-version', params['api-version'], {});

      rb.body(params.body, 'multipart/form-data');
    }
    return this.http.request(rb.build({
      responseType: 'text',
      accept: '*/*'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return (r as HttpResponse<any>).clone({ body: undefined }) as StrictHttpResponse<void>;
      })
    );
  }

  /**
   * Uploading new documents to the database.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `apiDocumentsPut$Response()` instead.
   *
   * This method sends `multipart/form-data` and handles request body of type `multipart/form-data`.
   */
  apiDocumentsPut(params?: {

    /**
     * The requested API version
     */
    'api-version'?: string;
      body?: { 'files': Array<Blob> }
  }): Observable<Either<ServerError, void>> {

    return this.apiDocumentsPut$Response(params).pipe(
      eitherify()
    );
  }
  /**
   * Path part for operation apiDocumentsDocumentIdGet
   */
  static readonly ApiDocumentsDocumentIdGetPath = '/api/Documents/{documentId}';

  /**
   * Retrieves the document.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `apiDocumentsDocumentIdGet$Plain()` instead.
   *
   * This method doesn't expect any request body.
   */
  apiDocumentsDocumentIdGet$Plain$Response(params: {
    documentId: null | string;

    /**
     * The requested API version
     */
    'api-version'?: string;

  }): Observable<StrictHttpResponse<Document>> {

    const rb = new RequestBuilder(this.rootUrl, tDocumentsService.ApiDocumentsDocumentIdGetPath, 'get');
    if (params) {

      rb.path('documentId', params.documentId, {});
      rb.query('api-version', params['api-version'], {});

    }
    return this.http.request(rb.build({
      responseType: 'text',
      accept: 'text/plain'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<Document>;
      })
    );
  }

  /**
   * Retrieves the document.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `apiDocumentsDocumentIdGet$Plain$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  apiDocumentsDocumentIdGet$Plain(params: {
    documentId: null | string;

    /**
     * The requested API version
     */
    'api-version'?: string;

  }): Observable<Either<ServerError, Document>> {

    return this.apiDocumentsDocumentIdGet$Plain$Response(params).pipe(
      eitherify()
    );
  }
  /**
   * Retrieves the document.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `apiDocumentsDocumentIdGet$Json()` instead.
   *
   * This method doesn't expect any request body.
   */
  apiDocumentsDocumentIdGet$Json$Response(params: {
    documentId: null | string;

    /**
     * The requested API version
     */
    'api-version'?: string;

  }): Observable<StrictHttpResponse<Document>> {

    const rb = new RequestBuilder(this.rootUrl, tDocumentsService.ApiDocumentsDocumentIdGetPath, 'get');
    if (params) {

      rb.path('documentId', params.documentId, {});
      rb.query('api-version', params['api-version'], {});

    }
    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'text/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<Document>;
      })
    );
  }

  /**
   * Retrieves the document.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `apiDocumentsDocumentIdGet$Json$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  apiDocumentsDocumentIdGet$Json(params: {
    documentId: null | string;

    /**
     * The requested API version
     */
    'api-version'?: string;

  }): Observable<Either<ServerError, Document>> {

    return this.apiDocumentsDocumentIdGet$Json$Response(params).pipe(
      eitherify()
    );
  }
  /**
   * Path part for operation apiDocumentsDocumentIdPost
   */
  static readonly ApiDocumentsDocumentIdPostPath = '/api/Documents/{documentId}';

  /**
   * Updating the existing document.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `apiDocumentsDocumentIdPost()` instead.
   *
   * This method sends `multipart/form-data` and handles request body of type `multipart/form-data`.
   */
  apiDocumentsDocumentIdPost$Response(params: {
    documentId: null | string;

    /**
     * The requested API version
     */
    'api-version'?: string;
      body?: { 'files'?: Array<Blob> }
  }): Observable<StrictHttpResponse<void>> {

    const rb = new RequestBuilder(this.rootUrl, tDocumentsService.ApiDocumentsDocumentIdPostPath, 'post');
    if (params) {

      rb.path('documentId', params.documentId, {});
      rb.query('api-version', params['api-version'], {});

      rb.body(params.body, 'multipart/form-data');
    }
    return this.http.request(rb.build({
      responseType: 'text',
      accept: '*/*'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return (r as HttpResponse<any>).clone({ body: undefined }) as StrictHttpResponse<void>;
      })
    );
  }

  /**
   * Updating the existing document.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `apiDocumentsDocumentIdPost$Response()` instead.
   *
   * This method sends `multipart/form-data` and handles request body of type `multipart/form-data`.
   */
  apiDocumentsDocumentIdPost(params: {
    documentId: null | string;

    /**
     * The requested API version
     */
    'api-version'?: string;
      body?: { 'files'?: Array<Blob> }
  }): Observable<Either<ServerError, void>> {

    return this.apiDocumentsDocumentIdPost$Response(params).pipe(
      eitherify()
    );
  }
  /**
   * Path part for operation apiDocumentsDocumentIdDelete
   */
  static readonly ApiDocumentsDocumentIdDeletePath = '/api/Documents/{documentId}';

  /**
   * Deleting the document by id.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `apiDocumentsDocumentIdDelete()` instead.
   *
   * This method doesn't expect any request body.
   */
  apiDocumentsDocumentIdDelete$Response(params: {
    documentId: null | string;

    /**
     * The requested API version
     */
    'api-version'?: string;

  }): Observable<StrictHttpResponse<void>> {

    const rb = new RequestBuilder(this.rootUrl, tDocumentsService.ApiDocumentsDocumentIdDeletePath, 'delete');
    if (params) {

      rb.path('documentId', params.documentId, {});
      rb.query('api-version', params['api-version'], {});

    }
    return this.http.request(rb.build({
      responseType: 'text',
      accept: '*/*'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return (r as HttpResponse<any>).clone({ body: undefined }) as StrictHttpResponse<void>;
      })
    );
  }

  /**
   * Deleting the document by id.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `apiDocumentsDocumentIdDelete$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  apiDocumentsDocumentIdDelete(params: {
    documentId: null | string;

    /**
     * The requested API version
     */
    'api-version'?: string;

  }): Observable<Either<ServerError, void>> {

    return this.apiDocumentsDocumentIdDelete$Response(params).pipe(
      eitherify()
    );
  }
}