/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';
import { RequestBuilder } from '../request-builder';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';
import { eitherify } from '../../domain/functions';
import { ServerError } from '../../domain/errors';
import { Either } from 'fp-ts/Either';


import { UserCreationRequest } from '../models/user-creation-request';
import { UserLoginRequest } from '../models/user-login-request';

@Injectable({
  providedIn: 'root',
})
export class tUsersService extends BaseService {
  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Path part for operation apiUsersExistsGet
   */
  static readonly ApiUsersExistsGetPath = '/api/Users/exists';

  /**
   * Checks if user name already taken.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `apiUsersExistsGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  apiUsersExistsGet$Response(params?: {
    userName?: null | string;

    /**
     * The requested API version
     */
    'api-version'?: string;

  }): Observable<StrictHttpResponse<void>> {

    const rb = new RequestBuilder(this.rootUrl, tUsersService.ApiUsersExistsGetPath, 'get');
    if (params) {

      rb.query('userName', params.userName, {});
      rb.query('api-version', params['api-version'], {});

    }
    return this.http.request(rb.build({
      responseType: 'text',
      accept: '*/*'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return (r as HttpResponse<any>).clone({ body: undefined }) as StrictHttpResponse<void>;
      })
    );
  }

  /**
   * Checks if user name already taken.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `apiUsersExistsGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  apiUsersExistsGet(params?: {
    userName?: null | string;

    /**
     * The requested API version
     */
    'api-version'?: string;

  }): Observable<Either<ServerError, void>> {

    return this.apiUsersExistsGet$Response(params).pipe(
      eitherify()
    );
  }
  /**
   * Path part for operation apiUsersThrowGet
   */
  static readonly ApiUsersThrowGetPath = '/api/Users/throw';

  /**
   * Throws.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `apiUsersThrowGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  apiUsersThrowGet$Response(params?: {

    /**
     * The requested API version
     */
    'api-version'?: string;

  }): Observable<StrictHttpResponse<void>> {

    const rb = new RequestBuilder(this.rootUrl, tUsersService.ApiUsersThrowGetPath, 'get');
    if (params) {

      rb.query('api-version', params['api-version'], {});

    }
    return this.http.request(rb.build({
      responseType: 'text',
      accept: '*/*'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return (r as HttpResponse<any>).clone({ body: undefined }) as StrictHttpResponse<void>;
      })
    );
  }

  /**
   * Throws.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `apiUsersThrowGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  apiUsersThrowGet(params?: {

    /**
     * The requested API version
     */
    'api-version'?: string;

  }): Observable<Either<ServerError, void>> {

    return this.apiUsersThrowGet$Response(params).pipe(
      eitherify()
    );
  }
  /**
   * Path part for operation apiUsersPut
   */
  static readonly ApiUsersPutPath = '/api/Users';

  /**
   * Creates the user.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `apiUsersPut()` instead.
   *
   * This method sends `application/*+json` and handles request body of type `application/*+json`.
   */
  apiUsersPut$Response(params?: {

    /**
     * The requested API version
     */
    'api-version'?: string;
      body?: UserCreationRequest
  }): Observable<StrictHttpResponse<void>> {

    const rb = new RequestBuilder(this.rootUrl, tUsersService.ApiUsersPutPath, 'put');
    if (params) {

      rb.query('api-version', params['api-version'], {});

      rb.body(params.body, 'application/*+json');
    }
    return this.http.request(rb.build({
      responseType: 'text',
      accept: '*/*'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return (r as HttpResponse<any>).clone({ body: undefined }) as StrictHttpResponse<void>;
      })
    );
  }

  /**
   * Creates the user.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `apiUsersPut$Response()` instead.
   *
   * This method sends `application/*+json` and handles request body of type `application/*+json`.
   */
  apiUsersPut(params?: {

    /**
     * The requested API version
     */
    'api-version'?: string;
      body?: UserCreationRequest
  }): Observable<Either<ServerError, void>> {

    return this.apiUsersPut$Response(params).pipe(
      eitherify()
    );
  }
  /**
   * Path part for operation apiUsersLoginPut
   */
  static readonly ApiUsersLoginPutPath = '/api/Users/login';

  /**
   * Logins the specified user.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `apiUsersLoginPut$Plain()` instead.
   *
   * This method sends `application/*+json` and handles request body of type `application/*+json`.
   */
  apiUsersLoginPut$Plain$Response(params?: {

    /**
     * The requested API version
     */
    'api-version'?: string;
  
    /**
     * The login request.
     */
    body?: UserLoginRequest
  }): Observable<StrictHttpResponse<string>> {

    const rb = new RequestBuilder(this.rootUrl, tUsersService.ApiUsersLoginPutPath, 'put');
    if (params) {

      rb.query('api-version', params['api-version'], {});

      rb.body(params.body, 'application/*+json');
    }
    return this.http.request(rb.build({
      responseType: 'text',
      accept: 'text/plain'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<string>;
      })
    );
  }

  /**
   * Logins the specified user.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `apiUsersLoginPut$Plain$Response()` instead.
   *
   * This method sends `application/*+json` and handles request body of type `application/*+json`.
   */
  apiUsersLoginPut$Plain(params?: {

    /**
     * The requested API version
     */
    'api-version'?: string;
  
    /**
     * The login request.
     */
    body?: UserLoginRequest
  }): Observable<Either<ServerError, string>> {

    return this.apiUsersLoginPut$Plain$Response(params).pipe(
      eitherify()
    );
  }
  /**
   * Logins the specified user.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `apiUsersLoginPut$Json()` instead.
   *
   * This method sends `application/*+json` and handles request body of type `application/*+json`.
   */
  apiUsersLoginPut$Json$Response(params?: {

    /**
     * The requested API version
     */
    'api-version'?: string;
  
    /**
     * The login request.
     */
    body?: UserLoginRequest
  }): Observable<StrictHttpResponse<string>> {

    const rb = new RequestBuilder(this.rootUrl, tUsersService.ApiUsersLoginPutPath, 'put');
    if (params) {

      rb.query('api-version', params['api-version'], {});

      rb.body(params.body, 'application/*+json');
    }
    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'text/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<string>;
      })
    );
  }

  /**
   * Logins the specified user.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `apiUsersLoginPut$Json$Response()` instead.
   *
   * This method sends `application/*+json` and handles request body of type `application/*+json`.
   */
  apiUsersLoginPut$Json(params?: {

    /**
     * The requested API version
     */
    'api-version'?: string;
  
    /**
     * The login request.
     */
    body?: UserLoginRequest
  }): Observable<Either<ServerError, string>> {

    return this.apiUsersLoginPut$Json$Response(params).pipe(
      eitherify()
    );
  }
}