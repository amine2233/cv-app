/* tslint:disable */

/**
 * User login data.
 */
export interface UserLoginRequest {

  /**
   * Gets or sets the name.
   */
  name: string;

  /**
   * Gets or sets the password.
   */
  password: string;
}
