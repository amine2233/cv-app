/* tslint:disable */

/**
 * Document for the UI.
 */
export interface Document {

  /**
   * Gets or sets the author.
   */
  author?: null | string;
}
