/* tslint:disable */

/**
 * Request for user creation
 */
export interface UserCreationRequest {

  /**
   * Gets or sets the name.
   */
  name: string;

  /**
   * Gets or sets the password.
   */
  password: string;
}
