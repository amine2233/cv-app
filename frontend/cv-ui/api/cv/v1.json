{
  "openapi": "3.0.1",
  "info": {
    "title": "CV Project API",
    "description": "An application with CV editing functionality.<br />\r\n<br />5.3.1<br /> ",
    "contact": {
      "name": "Author",
      "email": "alice@somewhere.com"
    },
    "license": {
      "name": "MIT",
      "url": "https://opensource.org/licenses/MIT"
    },
    "version": "1.0"
  },
  "servers": [
    {
      "url": "http://example.com"
    }
  ],
  "paths": {
    "/api/Documents": {
      "get": {
        "tags": [
          "Documents"
        ],
        "summary": "Retrieves the list of documents.",
        "parameters": [
          {
            "name": "api-version",
            "in": "query",
            "description": "The requested API version",
            "schema": {
              "type": "string"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "The document list was successfully retrieved.",
            "content": {
              "text/plain": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/Document"
                  }
                }
              },
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/Document"
                  }
                }
              },
              "text/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/Document"
                  }
                }
              }
            }
          }
        },
        "security": [
          { }
        ]
      },
      "put": {
        "tags": [
          "Documents"
        ],
        "summary": "Uploading new documents to the database.",
        "parameters": [
          {
            "name": "api-version",
            "in": "query",
            "description": "The requested API version",
            "schema": {
              "type": "string"
            }
          }
        ],
        "requestBody": {
          "content": {
            "multipart/form-data": {
              "schema": {
                "required": [
                  "files"
                ],
                "type": "object",
                "properties": {
                  "files": {
                    "type": "array",
                    "items": {
                      "type": "string",
                      "format": "binary"
                    }
                  }
                }
              },
              "encoding": {
                "files": {
                  "style": "form"
                }
              }
            }
          }
        },
        "responses": {
          "200": {
            "description": "Documents have been successfully processed."
          }
        },
        "security": [
          { }
        ]
      }
    },
    "/api/Documents/{documentId}": {
      "get": {
        "tags": [
          "Documents"
        ],
        "summary": "Retrieves the document.",
        "parameters": [
          {
            "name": "documentId",
            "in": "path",
            "required": true,
            "schema": {
              "type": "string",
              "nullable": true
            }
          },
          {
            "name": "api-version",
            "in": "query",
            "description": "The requested API version",
            "schema": {
              "type": "string"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "The document was successfully retrieved.",
            "content": {
              "text/plain": {
                "schema": {
                  "$ref": "#/components/schemas/Document"
                }
              },
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/Document"
                }
              },
              "text/json": {
                "schema": {
                  "$ref": "#/components/schemas/Document"
                }
              }
            }
          }
        },
        "security": [
          { }
        ]
      },
      "delete": {
        "tags": [
          "Documents"
        ],
        "summary": "Deleting the document by id.",
        "parameters": [
          {
            "name": "documentId",
            "in": "path",
            "required": true,
            "schema": {
              "type": "string",
              "nullable": true
            }
          },
          {
            "name": "api-version",
            "in": "query",
            "description": "The requested API version",
            "schema": {
              "type": "string"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Document have been successfully deleted."
          },
          "400": {
            "description": "Validation failed."
          }
        },
        "security": [
          { }
        ]
      },
      "post": {
        "tags": [
          "Documents"
        ],
        "summary": "Updating the existing document.",
        "parameters": [
          {
            "name": "documentId",
            "in": "path",
            "required": true,
            "schema": {
              "type": "string",
              "nullable": true
            }
          },
          {
            "name": "api-version",
            "in": "query",
            "description": "The requested API version",
            "schema": {
              "type": "string"
            }
          }
        ],
        "requestBody": {
          "content": {
            "multipart/form-data": {
              "schema": {
                "type": "object",
                "properties": {
                  "files": {
                    "type": "array",
                    "items": {
                      "type": "string",
                      "format": "binary"
                    },
                    "nullable": true
                  }
                }
              },
              "encoding": {
                "files": {
                  "style": "form"
                }
              }
            }
          }
        },
        "responses": {
          "200": {
            "description": "Document have been successfully updated."
          },
          "400": {
            "description": "Validation failed."
          }
        },
        "security": [
          { }
        ]
      }
    },
    "/Info": {
      "get": {
        "tags": [
          "Info"
        ],
        "summary": "Retrieves the information from application.",
        "parameters": [
          {
            "name": "api-version",
            "in": "query",
            "description": "The requested API version",
            "schema": {
              "type": "string"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "The document was successfully retrieved.",
            "content": {
              "text/plain": {
                "schema": {
                  "type": "object",
                  "additionalProperties": {
                    "type": "object"
                  }
                }
              },
              "application/json": {
                "schema": {
                  "type": "object",
                  "additionalProperties": {
                    "type": "object"
                  }
                }
              },
              "text/json": {
                "schema": {
                  "type": "object",
                  "additionalProperties": {
                    "type": "object"
                  }
                }
              }
            }
          }
        },
        "security": [
          { }
        ]
      }
    },
    "/api/Users/exists": {
      "get": {
        "tags": [
          "Users"
        ],
        "summary": "Checks if user name already taken.",
        "parameters": [
          {
            "name": "userName",
            "in": "query",
            "schema": {
              "type": "string",
              "nullable": true
            }
          },
          {
            "name": "api-version",
            "in": "query",
            "description": "The requested API version",
            "schema": {
              "type": "string"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "The user name already taken."
          },
          "404": {
            "description": "The user name available."
          }
        },
        "security": [
          { }
        ]
      }
    },
    "/api/Users/throw": {
      "get": {
        "tags": [
          "Users"
        ],
        "summary": "Throws.",
        "parameters": [
          {
            "name": "api-version",
            "in": "query",
            "description": "The requested API version",
            "schema": {
              "type": "string"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Success"
          }
        },
        "security": [
          { }
        ]
      }
    },
    "/api/Users": {
      "put": {
        "tags": [
          "Users"
        ],
        "summary": "Creates the user",
        "parameters": [
          {
            "name": "api-version",
            "in": "query",
            "description": "The requested API version",
            "schema": {
              "type": "string"
            }
          }
        ],
        "requestBody": {
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/UserCreationRequest"
              }
            },
            "text/json": {
              "schema": {
                "$ref": "#/components/schemas/UserCreationRequest"
              }
            },
            "application/*+json": {
              "schema": {
                "$ref": "#/components/schemas/UserCreationRequest"
              }
            }
          }
        },
        "responses": {
          "200": {
            "description": "Success"
          },
          "201": {
            "description": "User was successfully created."
          },
          "400": {
            "description": "Validation failed."
          }
        },
        "security": [
          { }
        ]
      }
    },
    "/api/Users/login": {
      "put": {
        "tags": [
          "Users"
        ],
        "summary": "Logins the specified user.",
        "parameters": [
          {
            "name": "api-version",
            "in": "query",
            "description": "The requested API version",
            "schema": {
              "type": "string"
            }
          }
        ],
        "requestBody": {
          "description": "The login request.",
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/UserLoginRequest"
              }
            },
            "text/json": {
              "schema": {
                "$ref": "#/components/schemas/UserLoginRequest"
              }
            },
            "application/*+json": {
              "schema": {
                "$ref": "#/components/schemas/UserLoginRequest"
              }
            }
          }
        },
        "responses": {
          "200": {
            "description": "User passed validation.",
            "content": {
              "text/plain": {
                "schema": {
                  "type": "string"
                }
              },
              "application/json": {
                "schema": {
                  "type": "string"
                }
              },
              "text/json": {
                "schema": {
                  "type": "string"
                }
              }
            }
          },
          "400": {
            "description": "Validation failed."
          }
        },
        "security": [
          { }
        ]
      }
    }
  },
  "components": {
    "schemas": {
      "Document": {
        "type": "object",
        "properties": {
          "author": {
            "type": "string",
            "description": "Gets or sets the author.",
            "nullable": true
          }
        },
        "description": "Document for the UI."
      },
      "UserCreationRequest": {
        "required": [
          "name",
          "password"
        ],
        "type": "object",
        "properties": {
          "name": {
            "minLength": 1,
            "type": "string",
            "description": "Gets or sets the name."
          },
          "password": {
            "minLength": 1,
            "type": "string",
            "description": "Gets or sets the password."
          }
        },
        "description": "Request for user creation"
      },
      "UserLoginRequest": {
        "required": [
          "name",
          "password"
        ],
        "type": "object",
        "properties": {
          "name": {
            "minLength": 1,
            "type": "string",
            "description": "Gets or sets the name."
          },
          "password": {
            "minLength": 1,
            "type": "string",
            "description": "Gets or sets the password."
          }
        },
        "description": "User login data."
      }
    },
    "securitySchemes": {
      "oauth2": {
        "type": "apiKey",
        "description": "Standard Authorization header using the Bearer scheme. Example: \"Bearer {token}\"",
        "name": "Authorization",
        "in": "header"
      }
    }
  }
}