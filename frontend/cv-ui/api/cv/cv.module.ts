import { NgModule, ModuleWithProviders } from '@angular/core';
import { ApiConfiguration } from './generated/api-configuration';
import { ApiModule } from './generated/api.module';
import { DocumentService } from './services/document.service';
import { HostSettings } from '@cv/core';
import { InfoService } from './services/info.service';

const modules = [ApiModule];
const services = [DocumentService, InfoService];

export function cvTokenFactory(host: HostSettings): ApiConfiguration {
  const c = new ApiConfiguration();
  c.rootUrl = host.api;
  return c;
}

@NgModule({
  imports: [ApiModule.forRoot()],
  exports: modules,
  providers: [
    ...services,
    {
      provide: ApiConfiguration,
      useFactory: cvTokenFactory,
      deps: [HostSettings]
    }
  ]
})
export class ApiCvRootModule {}

@NgModule({
  exports: modules
})
export class ApiCvModule {
  static forRoot(): ModuleWithProviders<ApiCvRootModule> {
    return {
      ngModule: ApiCvRootModule
    };
  }
}
