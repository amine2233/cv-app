import * as t from 'io-ts';

const DocumentModel = t.intersection([
  t.type({
    author: t.string,
    name: t.string,
    id: t.string
  }),
  t.partial({
    extension: t.string
  })
]);

type DocumentModel = t.TypeOf<typeof DocumentModel>;
const DocumentModels = t.array(DocumentModel);
export { DocumentModel, DocumentModels };

const InfoModel = t.partial({
  key: t.any
});

type InfoModel = t.TypeOf<typeof InfoModel>;
const InfoModels = t.array(InfoModel);
export { InfoModel, InfoModels };
