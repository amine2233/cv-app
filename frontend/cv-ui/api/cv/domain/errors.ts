import { option as O } from 'fp-ts';

export interface ApiError {
  userFriendlyError: string;
  details: O.Option<string>;
}

export interface ServerError extends ApiError {}
export interface ValidationError extends ApiError {}
