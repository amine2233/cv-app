import { Observable, of, OperatorFunction } from 'rxjs';
import { StrictHttpResponse } from '../generated/strict-http-response';
import { Either, right, left, isLeft, map as eMap } from 'fp-ts/Either';
import { ServerError } from './errors';
import { map, catchError, tap } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { pipe } from 'fp-ts/function';

export function eitherify<T>(): (source: Observable<StrictHttpResponse<T>>) => Observable<Either<ServerError, T>> {
  return (source: Observable<StrictHttpResponse<T>>) =>
    source.pipe(
      map(r => right<ServerError, T>(r.body as T)),
      catchError(err =>
        of(
          left<ServerError, T>({
            userFriendlyError: err instanceof HttpErrorResponse ? err.message : 'Cannot load data.',
            details: err
          })
        )
      )
    );
}

export function tapEitherError<E, D>(tapData: (data: E) => void) {
  return tap<Either<E, D>>((either: Either<E, D>) => {
    if (isLeft(either)) {
      tapData(either.left);
    }
  });
}

export function log<T>(message?: string): OperatorFunction<T, T> {
  return tap(e => console.log(message, e));
}

export function mapEither<E, D, ND>(mapDataOrValue: ((data: D) => ND) | ND) {
  const mapDataResult =
    typeof mapDataOrValue === 'function' ? (mapDataOrValue as (data: D) => ND) : () => mapDataOrValue;
  return map<Either<E, D>, Either<E, ND>>((either: Either<E, D>) => pipe(either, eMap(mapDataResult)));
}
