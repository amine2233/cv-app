const escapeRegex = function (str) {
  return str.replace(/[-\\^$*+?.()|[\]{}]/g, '\\$&');
};

module.exports.pathsToModuleNameMapper = function (mapping, prefix = '') {
  const jestMap = {};
  for (const fromPath of Object.keys(mapping)) {
    let pattern;
    const toPaths = mapping[fromPath];
    // check that we have only one target path
    if (toPaths.length === 0) {
      continue;
    }
    const target = toPaths[0];

    // split with '*'
    const segments = fromPath.split(/\*/g);
    if (segments.length === 1) {
      pattern = `^${escapeRegex(fromPath)}$`;
      jestMap[pattern] = `${prefix}${target}`;
    } else if (segments.length === 2) {
      pattern = `^${escapeRegex(segments[0])}(.*)${escapeRegex(segments[1])}$`;
      jestMap[pattern] = `${prefix}${target.replace(/\*/g, '$1')}`;
    } else {
      continue;
    }
  }
  return jestMap;
};
