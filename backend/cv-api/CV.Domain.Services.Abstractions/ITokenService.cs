﻿// Copyright (c) 2019 under MIT license.

namespace CV.Domain.Services.Abstractions
{
    /// <summary>
    /// Token service.
    /// </summary>
    public interface ITokenService
    {
        /// <summary>
        /// Issuing Bearer token (string with Bearer)
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <returns></returns>
        string IssueBearerToken(string userName);
    }
}
