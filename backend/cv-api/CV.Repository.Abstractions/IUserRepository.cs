﻿// Copyright (c) 2019 under MIT license.

using CV.Domain.Models;

namespace CV.Repository.Abstractions
{
    /// <summary>
    /// User Repository.
    /// </summary>
    public interface IUserRepository
    {
        /// <summary>
        /// Determines whether user name can be taken.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <returns></returns>
        bool IsNameAvailable(string userName);

        /// <summary>
        /// Creating the user.
        /// </summary>
        User Save(User user);

        /// <summary>
        /// Gets the user.
        /// </summary>
        User GetByName(string userName);
    }
}
