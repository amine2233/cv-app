﻿// Copyright (c) 2019 under MIT license.

using CV.Domain.Models;
using Xunit;

namespace CV.Domain.Tests
{
    public class DocumentTests
    {
        [Fact]
        public void Document_HasId()
        {
            // Arrange/Act
            var document = new Document("name", "author", System.Array.Empty<byte>());

            // Assert
            Assert.False(string.IsNullOrWhiteSpace(document.Id));
        }
    }
}