﻿// Copyright (c) 2019 under MIT license.

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc.ApiExplorer;

namespace CV.WebApi.Extensions
{
    /// <summary>
    /// ApplicationBuilder extensions.
    /// </summary>
    public static class ApplicationExtensions
    {
        /// <summary>
        /// Uses the standard services (Swagger).
        /// </summary>
        /// <param name="app">The application builder.</param>
        /// <param name="provider">The API description provider.</param>
        public static void UseStandardServices(this IApplicationBuilder app, IApiVersionDescriptionProvider provider)
        {
            app.UseCors(builder => builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader()); // TODO: review CORS
            AttachSwagger(app, provider);
        }

        private static void AttachSwagger(this IApplicationBuilder app, IApiVersionDescriptionProvider provider)
        {
            app.UseSwagger();
            app.UseSwaggerUI(
                options =>
                {
                    // build a swagger endpoint for each discovered API version
                    foreach (var description in provider.ApiVersionDescriptions)
                    {
                        options.SwaggerEndpoint($"/swagger/{description.GroupName}/swagger.json", description.GroupName.ToUpperInvariant());
                    }
                });
        }
    }
}
