﻿// Copyright (c) 2019 under MIT license.

using CV.WebApi.Extensions.Contributors;
using Microsoft.Extensions.DependencyInjection;

namespace CV.WebApi.Extensions
{
    public static class ContributorExtensions
    {
        public static void AddInfoContributor<T>(this IServiceCollection services) where T : class, IInfoContributor
        {
            services.AddSingleton<IInfoContributor, T>();
        }

        public static void AddInfoActuator(this IServiceCollection services)
        {
            services.AddSingleton<InfoContributor>();
        }
    }
}