﻿// Copyright (c) 2019 under MIT license.

using System;
using System.Collections.Generic;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace CV.WebApi.Extensions
{
    /// <summary>
    /// Swagger filter fot OAuth2.
    /// </summary>
    /// <seealso cref="IOperationFilter" />
    public class SecurityRequirementsOperationFilter : IOperationFilter
    {
        /// <inheritdoc />
        public void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            if (operation == null)
            {
                return;
            }

            if (operation.Security == null)
            {
                operation.Security = new List<OpenApiSecurityRequirement>();
            }

            var oAuthRequirements = new OpenApiSecurityRequirement()
            {
                {
                    new OpenApiSecurityScheme() { BearerFormat = "oauth2" }, new List<string>()
                }
            };

            operation.Security.Add(oAuthRequirements);
        }
    }
}
