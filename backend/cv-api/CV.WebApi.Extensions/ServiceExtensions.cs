﻿// Copyright (c) 2019 under MIT license.

using CV.Domain;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace CV.WebApi.Extensions
{
    /// <summary>
    /// Extensions for WebApi project
    /// </summary>
    public static class ServiceExtensions
    {
        /// <summary>
        /// Adds the standard services (Swagger).
        /// </summary>
        /// <param name="services">The services.</param>
        /// <param name="xmlCommentsFilePath">The XML comments file path.</param>
        public static void AddStandardServices(this IServiceCollection services, string xmlCommentsFilePath)
        {
            AttachSwagger(services, xmlCommentsFilePath);
            AttachCloudServices(services);
            AttachAntiforgery(services);
        }

        public static void AddCorsPolicies(this IServiceCollection services)
        {
            string[] localHostOrigins = new string[]
            {
                "http://localhost:4200"
            };

            string[] productionHostOrigins = new string[]
            {
                "https://faircv.today"
            };

            services.AddCors(options => // CORS middleware must precede any defined endpoints
            {
                options.AddPolicy(WebApiConstants.DevelopmentCorsPolicy, builder =>
                {
                    builder.WithOrigins(localHostOrigins)
                        .AllowAnyHeader().AllowAnyMethod();
                });
                options.AddPolicy(WebApiConstants.ProductionCorsPolicy, builder =>
                {
                    builder.WithOrigins(productionHostOrigins)
                        .AllowAnyHeader().AllowAnyMethod();
                });
            });
        }

        private static void AttachCloudServices(this IServiceCollection services)
        {
            services.AddOptions();
        }

        private static void AttachSwagger(this IServiceCollection services, string xmlCommentsFilePath)
        {
            services.AddApiVersioning(options =>
            {
                options.AssumeDefaultVersionWhenUnspecified = true;
                options.ApiVersionSelector = new CurrentImplementationApiVersionSelector(options);
                // reporting api versions will return the headers "api-supported-versions" and "api-deprecated-versions"
                options.ReportApiVersions = true;
            });
            services.AddVersionedApiExplorer(options =>
            {
                // add the versioned api explorer, which also adds IApiVersionDescriptionProvider service
                // note: the specified format code will format the version as "'v'major[.minor][-status]"
                options.GroupNameFormat = "'v'VVV";

                // note: this option is only necessary when versioning by url segment. the SubstitutionFormat
                // can also be used to control the format of the API version in route templates
                options.SubstituteApiVersionInUrl = true;
            });
            services.AddTransient<IConfigureOptions<SwaggerGenOptions>, ConfigureSwaggerOptions>();
            services.AddSwaggerGen(opts =>
            {
                opts.AddSecurityDefinition("oauth2", new OpenApiSecurityScheme()
                {
                    Description = "Standard Authorization header using the Bearer scheme. Example: \"Bearer {token}\"",
                    In = ParameterLocation.Header,
                    Name = "Authorization",
                    Type = SecuritySchemeType.ApiKey
                });
                opts.OperationFilter<SecurityRequirementsOperationFilter>();
                // add a custom operation filter which sets default values
                opts.OperationFilter<SwaggerDefaultValues>();

                // integrate xml comments
                opts.IncludeXmlComments(xmlCommentsFilePath);
            });
        }

        private static void AttachAntiforgery(this IServiceCollection services)
        {
            services.AddAntiforgery(options =>
            {
                options.HeaderName = "X-XSRF-TOKEN";
            });
        }
    }
}
