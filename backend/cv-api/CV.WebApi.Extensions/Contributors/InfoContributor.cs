﻿// Copyright (c) 2019 under MIT license.

using System.Collections.Generic;
using Microsoft.AspNetCore.Hosting;

namespace CV.WebApi.Extensions.Contributors
{
    public class InfoContributor : IInfoContributor
    {
        private readonly IWebHostEnvironment _env;

        public InfoContributor(IWebHostEnvironment env)
        {
            _env = env;
        }

        public void Contribute(IInfoBuilder builder)
        {
            builder?.WithInfo(CreateDictionary());
        }

        protected virtual Dictionary<string, object> CreateDictionary()
        {
            return new Dictionary<string, object>()
            {
                { "Environment", _env.EnvironmentName }
            };
        }
    }
}