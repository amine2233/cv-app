﻿// Copyright (c) 2019 under MIT license.

using System.Collections.Generic;

namespace CV.WebApi.Extensions.Contributors
{
    /// <summary>
    /// Application Information Builder
    /// </summary>
    public interface IInfoBuilder
    {
        /// <summary>
        /// Add information based on single key.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        IInfoBuilder WithInfo(string key, object value);

        /// <summary>
        /// Add bulk information
        /// </summary>
        /// <param name="details"></param>
        /// <returns></returns>
        IInfoBuilder WithInfo(Dictionary<string, object> details);

        /// <summary>
        /// build whole information based on already registered contributors.
        /// </summary>
        /// <returns></returns>
        Dictionary<string, object> Build();
    }
}