﻿// Copyright (c) 2019 under MIT license.

using System.Diagnostics;
using System.Reflection;

namespace CV.WebApi.Extensions.Contributors
{
    public class GitInfoContributor : IInfoContributor
    {
        public void Contribute(IInfoBuilder builder)
        {
            builder.WithInfo("Build revision",
                FileVersionInfo.GetVersionInfo(Assembly.GetEntryAssembly()?.Location).ProductVersion);
        }
    }
}