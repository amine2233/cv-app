﻿// Copyright (c) 2019 under MIT license.

using System.Collections.Generic;

namespace CV.WebApi.Extensions.Contributors
{
    public class InfoBuilder : IInfoBuilder
    {
        private readonly Dictionary<string, object> info = new Dictionary<string, object>();

        public Dictionary<string, object> Build()
        {
            return info;
        }

        public IInfoBuilder WithInfo(string key, object value)
        {
            if (!string.IsNullOrEmpty(key))
            {
                info[key] = value;
            }

            return this;
        }

        public IInfoBuilder WithInfo(Dictionary<string, object> items)
        {
            if (items != null)
            {
                foreach (var pair in items)
                {
                    info[pair.Key] = pair.Value;
                }
            }

            return this;
        }
    }
}