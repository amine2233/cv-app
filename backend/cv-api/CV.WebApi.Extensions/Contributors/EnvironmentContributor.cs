﻿// Copyright (c) 2019 under MIT license.

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace CV.WebApi.Extensions.Contributors
{
    public class EnvironmentContributor : IInfoContributor
    {
        public void Contribute(IInfoBuilder builder)
        {
            builder?.WithInfo(CreateDictionary());
        }

        protected virtual Dictionary<string, object> CreateDictionary()
        {
            return new Dictionary<string, object>(Environment.GetEnvironmentVariables().Cast<DictionaryEntry>()
                .Select(e => new KeyValuePair<string, object>(e.Key?.ToString(), e.Value)));
        }
    }
}