﻿// Copyright (c) 2019 under MIT license.

namespace CV.WebApi.Extensions.Contributors
{
    /// <summary>
    /// Interface for any Info conributor.
    /// </summary>
    public interface IInfoContributor
    {
        /// <summary>
        /// Contributor.
        /// </summary>
        /// <param name="builder"></param>
        void Contribute(IInfoBuilder builder);
    }
}