﻿// Copyright (c) 2019 under MIT license.

using CV.Domain.Services.Abstractions;
using Microsoft.Extensions.DependencyInjection;

namespace CV.Domain.Services
{
    /// <summary>
    /// Services module for Domain layer.
    /// </summary>
    public class DomainServicesModule : ServiceCollection
    {
        public DomainServicesModule()
        {
            this.AddSingleton<IPasswordHasher, PasswordHasher>();
            this.AddSingleton<IUserService, UserService>();
            this.AddSingleton<IDocumentService, DocumentService>();
            this.AddSingleton<ITokenService, TokenService>();
        }
    }
}