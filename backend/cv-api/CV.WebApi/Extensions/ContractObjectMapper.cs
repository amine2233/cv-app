﻿// Copyright (c) 2019 under MIT license.

using CV.Domain.Models;
using VM=CV.WebApi.V1.Contracts;

namespace CV.WebApi.Extensions
{
    public static class ContractObjectMapper
    {
        public static VM.Document ToContract(this Document document) => new VM.Document(document.Author);
    }
}