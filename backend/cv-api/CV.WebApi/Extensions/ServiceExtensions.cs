﻿// Copyright (c) 2019 under MIT license.

using System;
using CV.Domain;
using CV.WebApi.Infrastructure;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace CV.WebApi.Extensions
{
    /// <summary>
    /// Extension methods for <see cref="Microsoft.Extensions.DependencyInjection.IServiceCollection" /> object
    /// </summary>
    public static class ServiceExtensions
    {
        /// <summary>
        /// Register a validatable setting/>
        /// </summary>
        /// <typeparam name="TValidatableSettings">Validatable setting type</typeparam>
        /// <param name="services">Service collection.</param>
        /// <param name="section">Configuration section.</param>
        /// <returns></returns>
        public static IServiceCollection AddSetting<TValidatableSettings>(
            this IServiceCollection services,
            IConfigurationSection section)
            where TValidatableSettings : class, IValidatable, new()
        {
            if (section == null)
            {
                throw new ArgumentNullException(nameof(section), "Cannot load configuration for " + typeof(TValidatableSettings).Name + ".");
            }

            services.Configure<TValidatableSettings>(section);
            services.AddSingleton(resolver => resolver.GetRequiredService<IOptions<TValidatableSettings>>().Value);
            services.AddSingleton(resolver => (IValidatable)resolver.GetRequiredService<IOptions<TValidatableSettings>>().Value);
            return services;
        }

        /// <summary>
        /// Register validator class to validate all validatable objects on application startup.
        /// </summary>
        /// <param name="services">The services.</param>
        /// <returns></returns>
        public static IServiceCollection AddSettingsValidatorActuator(
            this IServiceCollection services)
        {
            services.AddTransient<IStartupFilter, ConfigurationValidationStartupFilter>();
            return services;
        }
    }
}
