﻿// Copyright (c) 2019 under MIT license.

using Microsoft.AspNetCore.Mvc;

namespace CV.WebApi.V1
{
    /// <summary>
    /// Base class for REST controllers.
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.ControllerBase" />
    public class BaseController : ControllerBase
    {
        /// <summary>
        /// Return user's name for the current request.
        /// </summary>
        /// <returns>User name</returns>
        protected string GetUser()
        {
            return User?.Identity.Name;
        }
    }
}
