﻿// Copyright (c) 2019 under MIT license.

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CV.Domain.Services.Abstractions;
using CV.WebApi.Constants;
using CV.WebApi.Extensions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using VM = CV.WebApi.V1.Contracts;
using DM = CV.Domain.Models;

namespace CV.WebApi.V1.Controllers
{
    /// <summary>
    /// Represents a RESTful Values service.
    /// </summary>
    [ApiVersion("1.0")]
    [Route(ServiceConstants.DefaultControllerRoute)]
    [ApiController]
    public class DocumentsController : BaseController
    {
        private readonly IDocumentService _documentService;

        /// <inheritdoc />
        public DocumentsController(IDocumentService documentService)
        {
            _documentService = documentService;
        }

        /// <summary>
        /// Retrieves the list of documents.
        /// </summary>
        /// <returns>The list of requested documents.</returns>
        /// <response code="200">The document list was successfully retrieved.</response>
        [HttpGet]
        public ActionResult<IEnumerable<VM.Document>> GetDocuments()
        {
            return Ok(_documentService.GetAll().Select(e => e.ToContract()));
        }

        /// <summary>
        /// Retrieves the document.
        /// </summary>
        /// <response code="200">The document was successfully retrieved.</response>
        [HttpGet("{documentId}")]
        public ActionResult<VM.Document> Get(string documentId)
        {
            return _documentService.GetById(documentId)
                .Match<ActionResult>(some => File(some.Data, "application/pdf"), NotFound);
        }

        /// <summary>
        /// Uploading new documents to the database.
        /// </summary>
        /// <response code="200">Documents have been successfully processed.</response>
        [HttpPut]
        public async Task<ActionResult> Upload([Required]IFormFileCollection files)
        {
            if (files.Count == 0)
            {
                return BadRequest(ErrorMessageConstants.NoFilesAttached);
            }

            var documents = await LoadDocuments(files);
            _documentService.Save(documents);
            return Ok();
        }

        /// <summary>
        /// Deleting the document by id.
        /// </summary>
        /// <response code="200">Document have been successfully deleted.</response>
        /// <response code="400">Validation failed.</response>
        [HttpDelete("{documentId}")]
        public ActionResult Delete(string documentId)
        {
            var actionResult = _documentService.Delete(documentId, GetUser());
            return actionResult == ValidationResult.Success
                ? (ActionResult)Ok()
                : BadRequest(actionResult);
        }

        /// <summary>
        /// Updating the existing document.
        /// </summary>
        /// <response code="200">Document have been successfully updated.</response>
        /// <response code="400">Validation failed.</response>
        [HttpPost("{documentId}")]
        public async Task<ActionResult> Update(string documentId, IFormFileCollection files)
        {
            if (files?.Count != 1)
            {
                return BadRequest(ErrorMessageConstants.OnlyOneFileAllowed);
            }

            var documents = await LoadDocuments(files);
            var actionResult = _documentService.Update(
                documentId, documents.Single(), GetUser());
            return actionResult == ValidationResult.Success
                ? (ActionResult)Ok()
                : BadRequest(actionResult);
        }

        private async Task<List<DM.Document>> LoadDocuments(IFormFileCollection files)
        {
            var documents = new List<DM.Document>();
            foreach (var file in files)
            {
                var content = await GetFileContent(file);
                documents.Add(new DM.Document(file.FileName, GetUser(), content));
            }

            return documents;
        }

        private static async Task<byte[]> GetFileContent(IFormFile file)
        {
            byte[] result;

            using (var stream = file.OpenReadStream())
            {
                using (var memoryStream = new MemoryStream())
                {
                    await stream.CopyToAsync(memoryStream);
                    result = memoryStream.ToArray();
                }
            }

            return result;
        }
    }
}
