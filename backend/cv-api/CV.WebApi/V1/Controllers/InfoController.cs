﻿// Copyright (c) 2019 under MIT license.

using System;
using System.Collections.Generic;
using CV.WebApi.Constants;
using CV.WebApi.Extensions.Contributors;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace CV.WebApi.V1.Controllers
{
    /// <summary>
    /// Represents a RESTful Values service.
    /// </summary>
    [ApiVersion("1.0")]
    [Route(ServiceConstants.RootControllerRoute)]
    [ApiController]
    [AllowAnonymous]
    public class InfoController : BaseController
    {
        private readonly IEnumerable<IInfoContributor> _contributors;
        private readonly ILogger<InfoController>? _logger;

        public InfoController(IEnumerable<IInfoContributor> contributors, ILogger<InfoController>? logger = null)
        {
            _contributors = contributors;
            _logger = logger;
        }

        /// <summary>
        /// Retrieves the information from application.
        /// </summary>
        /// <response code="200">The document was successfully retrieved.</response>
        [HttpGet]
        public ActionResult<Dictionary<string, object>> Get()
        {
            IInfoBuilder builder = new InfoBuilder();
            foreach (var contributor in _contributors)
            {
                try
                {
                    contributor.Contribute(builder);
                }
                catch (Exception e)
                {
                    _logger?.LogError("Exception: {0}", e);
                }
            }

            return Ok(builder.Build());
        }
    }
}