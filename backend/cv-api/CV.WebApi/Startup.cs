﻿// Copyright (c) 2019 under MIT license.

using System.IO;
using System.Reflection;
using CV.Core;
using CV.Domain;
using CV.Domain.Configurations;
using CV.Domain.Services;
using CV.Domain.Services.Abstractions;
using CV.Repository.Abstractions;
using CV.Repository.LiteDb;
using CV.Repository.LiteDb.Repositories;
using CV.WebApi.Extensions;
using CV.WebApi.Extensions.Contributors;
using CV.WebApi.Infrastructure;
using CV.WebApi.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.PlatformAbstractions;
using Serilog;

namespace CV.WebApi
{
    /// <summary>
    /// Startup class for the application.
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Startup"/> class.
        /// </summary>
        /// <param name="configuration">The configuration.</param>
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        private IConfiguration Configuration { get; }

        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        /// <param name="services">The services.</param>
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCorsPolicies();

            services
                .AddAuthentication(CustomAuthSchemaDefaults.AuthenticationScheme)
                .AddScheme<CustomAuthSchemaOptions, CustomAuthSchemaHandler>(CustomAuthSchemaDefaults.AuthenticationScheme, null);
            services
                .AddMvcCore(mvcOptions =>
                {
                    mvcOptions.ModelMetadataDetailsProviders.Add(new RequiredBindingMetadataProvider());
                    var policy = new AuthorizationPolicyBuilder()
                        .RequireAuthenticatedUser()
                        .Build();
                    mvcOptions.Filters.Add(new AuthorizeFilter(policy));
                });

            services.AddControllers();

            services.AddStandardServices(XmlCommentsFilePath);

            // register validatable settings
            services.AddSetting<LiteDbConfiguration>(
                Configuration.GetSection(WebApiConstants.LiteDb_CONFIGURATION_PREFIX));
            services.AddSetting<HashingOptions>(
                Configuration.GetSection(HashingConstants.CONFIGURATION_PREFIX));
            services.AddSetting<SecurityOptions>(
                Configuration.GetSection(WebApiConstants.Security_CONFIGURATION_PREFIX));

            services.AddSettingsValidatorActuator();

            services.Add<RepositoryServicesModule>();
            services.Add<DomainServicesModule>();

            services.AddInfoContributor<InfoContributor>();
            services.AddInfoContributor<GitInfoContributor>();
            services.AddInfoContributor<EnvironmentContributor>();
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app">The application.</param>
        /// <param name="env">The env.</param>
        /// <param name="provider">The provider.</param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IApiVersionDescriptionProvider provider)
        {
            app.UseSerilogRequestLogging(opts
                => opts.EnrichDiagnosticContext = LogHelper.EnrichFromRequest);
            if (env.IsDevelopment())
            {
                app.UseCors(WebApiConstants.DevelopmentCorsPolicy);
                app.UseDeveloperExceptionPage();
            }
            else if (env.IsProduction())
            {
                app.UseCors(WebApiConstants.ProductionCorsPolicy);
            }

            app.UseStandardServices(provider);

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGet(".well-known/acme-challenge/KI9I_adXR29LN2Aj9zDseRYLWxV7ivlUOothMbMYjB4",
                    async context =>
                        await context.Response.WriteAsync(
                            "KI9I_adXR29LN2Aj9zDseRYLWxV7ivlUOothMbMYjB4.MiReJnw_uM7Wh7aBSi7lIY27s43D81vCx_e7-akknEU"));
                endpoints.MapControllers();
            });
        }

        private static string XmlCommentsFilePath
        {
            get
            {
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var fileName = typeof(Startup).GetTypeInfo().Assembly.GetName().Name + ".xml";
                return Path.Combine(basePath, fileName);
            }
        }
    }
}
