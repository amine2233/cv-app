﻿// Copyright (c) 2019 under MIT license.

namespace CV.Core

open System.Runtime.CompilerServices
open Microsoft.Extensions.DependencyInjection

[<Extension>]
type ServiceCollectionExtensions =
    [<Extension>]
    static member inline Add<'T when 'T :> IServiceCollection and 'T: (new : unit -> 'T)>(services: IServiceCollection) = for d in new 'T() do services.Add(d)
