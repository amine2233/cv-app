﻿// Copyright (c) 2019 under MIT license.
namespace CV.Domain

module WebApiConstants = begin

    [<Literal>]
    let DevelopmentCorsPolicy = "DevelopmentCorsPolicy"
    
    [<Literal>]
    let ProductionCorsPolicy = "ProductionCorsPolicy"
    
    [<Literal>] 
    let LiteDb_CONFIGURATION_PREFIX = "LiteDb";
    
    [<Literal>]
    let Security_CONFIGURATION_PREFIX = "Security";

end 