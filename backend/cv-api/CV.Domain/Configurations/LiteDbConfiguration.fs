﻿// Copyright (c) 2019 under MIT license.

namespace CV.Domain.Configurations

open CV.Domain
open System.ComponentModel.DataAnnotations

[<Sealed>]
type LiteDbConfiguration() =
    inherit ConfigurationObjectBase()    
    [<Required; MinLength(1)>]
    member val DbPath = "" with get, set    
    [<Required; MinLength(1)>]
    member val Password = "" with get, set
   
