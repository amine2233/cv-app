﻿// Copyright (c) 2019 under MIT license.

namespace CV.Domain.Configurations
open System.ComponentModel;
open CV.Domain

[<AbstractClass; Sealed>]
type HashingConstants private () =
    static member CONFIGURATION_PREFIX = "Hashing"

[<Sealed>]
type HashingOptions() =
    inherit ConfigurationObjectBase()    
    [<DefaultValue(100000)>]
    member val Iterations = 100000 with get, set // 100ms
