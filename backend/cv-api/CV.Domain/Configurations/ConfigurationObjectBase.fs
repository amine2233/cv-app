﻿// Copyright (c) 2019 under MIT license.

namespace CV.Domain

open System.ComponentModel.DataAnnotations;

type IValidatable = 
    abstract member Validate: unit -> unit

type ConfigurationObjectBase() =
    interface IValidatable with
        member this.Validate() = Validator.ValidateObject(this, new ValidationContext(this, null, null), true)
