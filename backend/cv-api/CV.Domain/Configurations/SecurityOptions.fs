﻿// Copyright (c) 2019 under MIT license.
namespace CV.Domain.Configurations

open System.ComponentModel.DataAnnotations
open CV.Domain

type SecurityOptions() =
    inherit ConfigurationObjectBase()
     [<Required; MinLength(20)>]
    member val Secret = "" with get, set
