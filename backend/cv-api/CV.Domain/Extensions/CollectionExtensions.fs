﻿// Copyright (c) 2019 under MIT license.

namespace CV.Domain.Extensions

open System.Runtime.CompilerServices
open System;

[<Extension>]
type CollectionExtensions =
    [<Extension>]
    static member inline ByteArrayCompare(a1: byte[], a2: ReadOnlySpan<byte>) = a1.AsSpan().SequenceEqual(a2)