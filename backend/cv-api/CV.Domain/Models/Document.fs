﻿// Copyright (c) 2019 under MIT license.

namespace CV.Domain.Models

open System;
open System.Collections.Generic;

type Document(name: string, author: string, data: byte[]) =
    member this.Name = name
    member this.Author = author
    member this.Data = data
    member val Id = Guid.NewGuid().ToString()
