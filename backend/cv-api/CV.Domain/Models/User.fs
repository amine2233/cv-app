﻿// Copyright (c) 2019 under MIT license.

namespace CV.Domain.Models

type User(name: string, passwordHash: string) =
    member this.Name = name
    member this.PasswordHash = passwordHash
