﻿// Copyright (c) 2019 under MIT license.

using System.Collections.Generic;
using System.Linq;
using System.Text;
using CV.Domain.Extensions;
using CV.Domain.Models;
using CV.Repository.LiteDb.Repositories;
using Xunit;

namespace CV.Repository.LiteDb.Tests
{
    public class DocumentRepositoryTests
    {
        [Fact]
        public void Insert_Test()
        {
            // Arrange
            var newItems = new List<Document>()
            {
                new Document("testDocument1", "testAuthor1", Encoding.UTF8.GetBytes("testContent1")),
                new Document("testDocument2", "testAuthor2", Encoding.UTF8.GetBytes("testContent2")),
            };
            var tc = CreateTestCandidate();

            // Act
            tc.Save(newItems);

            // Assert
            Assert.Equal(2, tc.GetAll().Count());
            Assert.Contains(tc.GetAll(), i => i.Name == "testDocument1");
            Assert.Contains(tc.GetAll(), i => i.Author == "testAuthor1");
            Assert.Contains(tc.GetAll(), i => i.Data.ByteArrayCompare(Encoding.UTF8.GetBytes("testContent1")));
        }

        [Fact]
        public void Delete_Test()
        {
            // Arrange
            var newItems = new List<Document>()
            {
                new Document("testDocument1", "testAuthor1", Encoding.UTF8.GetBytes("testContent1")),
                new Document("testDocument2", "testAuthor2", Encoding.UTF8.GetBytes("testContent2")),
            };
            var tc = CreateTestCandidate();

            // Act / Assert
            tc.Save(newItems);
            Assert.Equal(2, tc.GetAll().Count());
            Assert.True(tc.Delete(newItems[0].Id));

            // Assert
            Assert.Single(tc.GetAll());
            Assert.Contains(tc.GetAll(), i => i.Name == "testDocument2");
            Assert.Contains(tc.GetAll(), i => i.Author == "testAuthor2");
            Assert.Contains(tc.GetAll(), i => i.Data.ByteArrayCompare(Encoding.UTF8.GetBytes("testContent2")));
        }

        [Fact]
        public void Update_Test()
        {
            // Arrange
            var newItems = new List<Document>()
            {
                new Document("testDocument1", "testAuthor1", Encoding.UTF8.GetBytes("testContent1"))
            };
            var tc = CreateTestCandidate();

            // Act / Assert
            tc.Save(newItems);
            Assert.Single(tc.GetAll());
            tc.Update(newItems[0].Id, new Document("any", "newAuthor", Encoding.UTF8.GetBytes("newContent")));

            // Assert
            Assert.Single(tc.GetAll());
            Assert.Contains(tc.GetAll(), i => i.Name == "any");
            Assert.Contains(tc.GetAll(), i => i.Author == "newAuthor");
            Assert.Contains(tc.GetAll(), i => i.Data.ByteArrayCompare(Encoding.UTF8.GetBytes("newContent")));
        }

        private DocumentRepository CreateTestCandidate()
        {
            return new DocumentRepository(new TestDbFactory());
        }
    }
}