﻿// Copyright (c) 2019 under MIT license.

using System.IO;
using CV.Repository.LiteDb.Repositories;
using LiteDB;

namespace CV.Repository.LiteDb.Tests
{
    public class TestDbFactory : IDbFactory
    {
        private readonly MemoryStream _stream = new MemoryStream();

        public LiteDatabase Create()
        {
            return new LiteDatabase(_stream, new BsonMapper());
        }
    }
}
