﻿// Copyright (c) 2019 under MIT license.

using CV.Repository.Abstractions;
using CV.Repository.LiteDb.Repositories;
using Microsoft.Extensions.DependencyInjection;

namespace CV.Repository.LiteDb
{
    /// <summary>
    /// Services module for Repository library.
    /// </summary>
    public class RepositoryServicesModule : ServiceCollection
    {
        public RepositoryServicesModule()
        {
            this.AddSingleton<IDbFactory, DbFactory>();
            this.AddSingleton<IDocumentRepository, DocumentRepository>();
            this.AddSingleton<IUserRepository, UserRepository>();
        }
    }
}