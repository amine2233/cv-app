﻿// Copyright (c) 2019 under MIT license.

using LiteDB;

namespace CV.Repository.LiteDb.Repositories
{
    /// <summary>
    /// Base repository class.
    /// </summary>
    internal class RepositoryBase
    {
        private readonly IDbFactory _dbFactory;

        /// <inheritdoc />
        public RepositoryBase(IDbFactory dbFactory)
        {
            _dbFactory = dbFactory;
        }

        /// <summary>
        /// Gets the LiteDb database instance.
        /// </summary>
        /// <returns></returns>
        protected LiteDatabase GetDatabase()
        {
            return _dbFactory.Create();
        }
    }
}
