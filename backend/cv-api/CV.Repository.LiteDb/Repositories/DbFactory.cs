﻿// Copyright (c) 2019 under MIT license.

using CV.Domain.Configurations;
using LiteDB;

namespace CV.Repository.LiteDb.Repositories
{
    /// <summary>
    /// Allows to create database instance.
    /// </summary>
    internal interface IDbFactory
    {
        /// <summary>
        /// Creates db instance.
        /// </summary>
        LiteDatabase Create();
    }

    /// <summary>
    /// Db Factory.
    /// </summary>
    /// <seealso cref="CV.Repository.LiteDb.Repositories.IDbFactory" />
    internal class DbFactory : IDbFactory
    {
        private readonly LiteDbConfiguration _configuration;

        /// <inheritdoc />
        public DbFactory(LiteDbConfiguration configuration)
        {
            _configuration = configuration;
        }

        /// <inheritdoc />
        public LiteDatabase Create()
        {
            var connectionString = new ConnectionString(_configuration.DbPath)
            {
                Password = _configuration.Password
            };
            return new LiteDatabase(connectionString);
        }
    }
}
