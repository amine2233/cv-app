﻿// Copyright (c) 2019 under MIT license.

using CV.Domain.Models;
using CV.Repository.Abstractions;
using CV.Repository.LiteDb.Entities;

namespace CV.Repository.LiteDb.Repositories
{
    /// <inheritdoc />
    internal class UserRepository : RepositoryBase, IUserRepository
    {
        /// <inheritdoc />
        public UserRepository(IDbFactory factory) : base(factory)
        {
        }

        /// <inheritdoc />
        public bool IsNameAvailable(string userName)
        {
            using (var db = GetDatabase())
            {
                var users = db.GetCollection<UserEntity>();
                return users.FindOne(u => u.Name == userName) == null;
            }
        }

        /// <inheritdoc />
        public User Save(User user)
        {
            var dbUser = user.ToEntity();
            using (var db = GetDatabase())
            {
                var users = db.GetCollection<UserEntity>();
                users.Insert(dbUser);
                return dbUser.ToDomain();
            }
        }

        /// <inheritdoc />
        public User GetByName(string userName)
        {
            using (var db = GetDatabase())
            {
                var users = db.GetCollection<UserEntity>();
                return users.FindById(userName).ToDomain();
            }
        }
    }
}
