﻿// Copyright (c) 2019 under MIT license.

using System.Collections.Generic;
using System.Linq;
using CV.Domain.Models;
using CV.Repository.Abstractions;
using CV.Repository.LiteDb.Entities;
using LanguageExt;

namespace CV.Repository.LiteDb.Repositories
{
    /// <inheritdoc />
    internal class DocumentRepository : RepositoryBase, IDocumentRepository
    {
        /// <inheritdoc />
        public DocumentRepository(IDbFactory factory)
            : base(factory)
        {
        }

        /// <inheritdoc />
        public void Save(IList<Document> documents)
        {
            using var db = GetDatabase();
            var docCollection = db.GetCollection<DocumentEntity>();
            docCollection.Insert(documents.Select(e => e.ToEntity()));
        }

        /// <inheritdoc />
        public IEnumerable<Document> GetAll()
        {
            using var db = GetDatabase();
            var docCollection = db.GetCollection<DocumentEntity>();
            return docCollection.FindAll().Select(i => i.ToDomain()).ToList();
        }

        /// <inheritdoc />
        public Option<Document> GetById(string id)
        {
            using var db = GetDatabase();
            var docCollection = db.GetCollection<DocumentEntity>();
            var dbItem = docCollection.FindById(id);
            return dbItem.ToDomain();
        }

        /// <inheritdoc />
        public bool Update(string documentId, Document document)
        {
            var dbItem = document.ToEntity();
            dbItem.Id = documentId; // TODO: Lonli-Lokli: remove!
            using var db = GetDatabase();
            var docCollection = db.GetCollection<DocumentEntity>();
            return docCollection.Update(dbItem);
        }

        /// <inheritdoc />
        public bool Delete(string id)
        {
            using var db = GetDatabase();
            var docCollection = db.GetCollection<DocumentEntity>();
            return docCollection.Delete(id);
        }
    }
}
