﻿// Copyright (c) 2019 under MIT license.

using System;
using CV.Domain.Models;
using CV.Repository.LiteDb.Entities;
using LanguageExt;

namespace CV.Repository.LiteDb
{
    internal static class DomainObjectMapper
    {
        public static Document? ToDomain(this DocumentEntity? document) =>
            document != null ? new Document(document.Name, document.Author, document.Data) : null;

        public static User ToDomain(this UserEntity? user) => new User(user.Name, user.PasswordHash);

        public static DocumentEntity ToEntity(this Document? document) => new DocumentEntity(document.Id,
            document.Name, document.Author, document.Data);

        public static UserEntity ToEntity(this User? user) => new UserEntity(user.Name, user.PasswordHash);
    }
}