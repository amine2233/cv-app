﻿// Copyright (c) 2019 under MIT license.

using System.Collections.Generic;
using LiteDB;

namespace CV.Repository.LiteDb.Entities
{
    /// <summary>
    /// LiteDb document entity.
    /// </summary>
    internal class DocumentEntity
    {
        /// <summary>
        /// Gets or sets the identifier of the document.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        [BsonId]
        public string? Id { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string? Name { get; }

        /// <summary>
        /// Gets or sets the author of the document.
        /// </summary>
        /// <value>
        /// The author.
        /// </value>
        public string? Author { get; }

        /// <summary>
        /// Gets or sets the data.
        /// </summary>
        /// <value>
        /// The data.
        /// </value>
        public byte[] Data { get; }

        public DocumentEntity(string? id, string? name, string? author, byte[] data)
        {
            Id = id;
            Name = name;
            Author = author;
            Data = data;
        }
    }
}
