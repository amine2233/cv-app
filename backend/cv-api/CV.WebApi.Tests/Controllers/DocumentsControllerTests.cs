﻿// Copyright (c) 2019 under MIT license.

using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using CV.WebApi.V1.Controllers;
using Xunit;

namespace CV.WebApi.Tests.Controllers
{
    public class DocumentsControllerTests : IClassFixture<TestWebApplicationFactory<Startup>>
    {
        private readonly TestWebApplicationFactory<Startup> _factory;

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentsControllerTests"/> class.
        /// </summary>
        /// <param name="factory">The factory.</param>
        public DocumentsControllerTests(TestWebApplicationFactory<Startup> factory)
        {
            _factory = factory;
        }

        /// <summary>
        /// Ensures that <see cref="DocumentsController.GetDocuments"/> returns correct status code.
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task GetDocuments_ReturnsCorrectStatusCode()
        {
            // Arrange
            var provider = TestClaimsProvider.WithUserClaims();
            var client = _factory.CreateClientWithTestAuth(provider);

            HttpResponseMessage? resultResponseMessage = await client.GetAsync("/api/Documents");

            // Assert
            Assert.Equal(HttpStatusCode.OK, resultResponseMessage.StatusCode);
        }

        /// <summary>
        /// Ensures that <see cref="DocumentsController.Get"/> returns correct status code.
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task GetUnknownDocument_ReturnsNotFound()
        {
            // Arrange
            var provider = TestClaimsProvider.WithUserClaims();
            var client = _factory.CreateClientWithTestAuth(provider);

            HttpResponseMessage? resultResponseMessage = await client.GetAsync("/api/Documents/abracadabra");

            // Assert
            Assert.Equal(HttpStatusCode.NotFound, resultResponseMessage.StatusCode);
        }
    }
}