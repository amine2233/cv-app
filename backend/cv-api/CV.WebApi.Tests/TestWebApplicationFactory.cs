﻿// Copyright (c) 2019 under MIT license.

using Microsoft.AspNetCore.Mvc.Testing;

namespace CV.WebApi.Tests
{
    // <inheritdoc />
    public class TestWebApplicationFactory<TStartup> : WebApplicationFactory<TStartup> where TStartup : class
    {
    }
}