variable "cluster_id" {
  description = "The ECS cluster ID"
  type        = string
}

variable "cluster_name" {
  description = "The ECS cluster name"
  type        = string
}

variable "service" {
  description = "The name of the service in the ECS cluster"
  type        = string
}

variable "region" {
  description = "Region to be propogated"
  type        = string
}

variable "api_env_s3_bucketname" {
  description = "API environment S3 bucket name"
  type        = string
}

variable "api_env_filename" {
  description = "API environment file name"
  type        = string
}

variable "task_execution_role_arn" {
  description = "Task execution IAM role ARN"
  type        = string
}