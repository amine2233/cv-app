provider "aws" {
  region = local.region
}

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "~> 3.1.0"
  name = local.resources_name

  cidr = "10.1.0.0/16"

  azs             = ["${local.region}a", "${local.region}b"]
  private_subnets = []
  public_subnets  = ["10.1.11.0/24", "10.1.12.0/24"]
  enable_dns_support   = true
  enable_dns_hostnames = true
  enable_nat_gateway   = false

  tags = {
    Terraform   = "yes"
    Owner       = local.owner
    Namespace   = local.namespace
    Environment = local.stage
    Usage       = local.usage
  }

  vpc_tags = {
    Name = "ECS ${local.namespace} (${local.stage})"
  }
}

module "security_group" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "~> 4.2.0"

  name        = "alb-sg-${random_pet.this.id}"
  description = "Security group for usage with ALB"
  vpc_id      = module.vpc.vpc_id

  ingress_cidr_blocks = ["0.0.0.0/0"]
  ingress_rules       = ["http-80-tcp","https-443-tcp", "all-icmp"]
  egress_rules        = ["all-all"]
}

module "alb" {
  source  = "terraform-aws-modules/alb/aws"
  version = "~> 6.2.0"

  name = "cv-api-alb"

  load_balancer_type = "application"

  vpc_id             = module.vpc.vpc_id
  subnets            = module.vpc.public_subnets
  security_groups    = [module.security_group.security_group_id]

  target_groups = [
    {
      name_prefix      = "pref-"
      backend_protocol = "HTTP"
      backend_port     = 8080
      target_type      = "instance",
      deregistration_delay = 10
      health_check = {
        enabled             = true
        interval            = 30
        path                = "/swagger"
        port                = "traffic-port"
        healthy_threshold   = 3
        unhealthy_threshold = 3
        timeout             = 6
        protocol            = "HTTP"
        matcher             = "200-399"
      }
    }
  ]

  https_listeners = [
    {
      port                 = 443
      protocol             = "HTTPS"
      # API CERTIFICATE SHOULD BE UPDATED HERE
      certificate_arn      = "arn:aws:acm:eu-west-2:769404502957:certificate/8568b65d-afb8-44d1-8acd-28311a5daa10"
      target_group_index   = 0
    }
  ]

  http_tcp_listeners = [
    {
      port        = 80
      protocol    = "HTTP"
      target_group_index = 0
      action_type = "redirect"
      redirect = {
        port        = "443"
        protocol    = "HTTPS"
        status_code = "HTTP_301"
      }
    }
  ]

  tags = {
    Environment = "Test"
  }
}

resource "random_pet" "this" {
  length = 2
}

#--- Shared Resources ---
#For now we only use the AWS ECS optimized ami <https://docs.aws.amazon.com/AmazonECS/latest/developerguide/ecs-optimized_AMI.html>
data "aws_ami" "amazon_linux_ecs" {

  owners = ["amazon"]
  filter {
    name   = "image-id"
    values = ["ami-077f98d933531f865"]
  }
}

data "aws_iam_policy_document" "api_bucket_policy" {
  statement {
    principals {
      type        = "AWS"
      identifiers = [aws_iam_role.ecs_service_role.arn]
    }

    actions = [
      "s3:ListBucket",
      "s3:GetObject",
      "s3:GetBucketLocation",
    ]

    resources = [
      "arn:aws:s3:::${local.api_env_s3_bucketname}",
      "arn:aws:s3:::${local.api_env_s3_bucketname}/*",
    ]
  }
}

#----- ECS --------
module "ecs" {
  source = "terraform-aws-modules/ecs/aws"
  name   = local.namespace
}

module "ec2-profile" {
  source = "terraform-aws-modules/ecs/aws//modules/ecs-instance-profile"
  name   = local.namespace
}



#----- ECS  Services--------

module "ecs_service" {
  source       = "./ecs_service"
  region       = local.region
  service      = local.ecs_service_name
  cluster_id   = module.ecs.ecs_cluster_id
  cluster_name = local.namespace
  task_execution_role_arn = aws_iam_role.ecs_service_role.arn
  api_env_filename = "vo_api_env.env"
  api_env_s3_bucketname = local.api_env_s3_bucketname
}

#---- S3 Bucket --------

module "s3_bucket" {
  source = "terraform-aws-modules/s3-bucket/aws"
  version = "~> 2.4.0"

  bucket = local.api_env_s3_bucketname
  acl    = "private"
  policy = data.aws_iam_policy_document.api_bucket_policy.json
  force_destroy = true
  attach_policy = true

  versioning = {
    enabled = true
  }

  # S3 bucket-level Public Access Block configuration
  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

#----- ECS  Resources -----
resource "aws_default_route_table" "default" {
  default_route_table_id = module.vpc.default_route_table_id

  tags = {
    Name        = "${local.resources_name}-private"
    Terraform   = "yes"
    Owner       = local.owner
    Namespace   = local.namespace
    Environment = local.stage
    Usage       = local.usage
  }
}

resource "aws_default_network_acl" "default" {
  default_network_acl_id = module.vpc.default_network_acl_id

  ingress {
    protocol   = -1
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }

  egress {
    protocol   = -1
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }

  tags = {
    Name        = local.resources_name
    Terraform   = "yes"
    Owner       = local.owner
    Namespace   = local.namespace
    Environment = local.stage
    Usage       = local.usage
  }

  lifecycle {
    ignore_changes = [subnet_ids]
  }
}

resource "aws_default_security_group" "default" {
  vpc_id = module.vpc.vpc_id

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name        = local.resources_name
    Terraform   = "yes"
    Owner       = local.owner
    Namespace   = local.namespace
    Environment = local.stage
    Usage       = local.usage
  }
}

resource "aws_iam_role" "ecs_service_role" {
  name = "ec2_iam_role"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",      
      "Sid": "",      
      "Principal": {
        "Service": [
          "ec2.amazonaws.com",
          "s3.amazonaws.com",
          "ecs-tasks.amazonaws.com"
        ]
      },
      "Action": "sts:AssumeRole"
    }  
  ]
}
EOF
}

resource "aws_iam_policy" "ecs_service_logging" {
  name = "ecs_service_logging"
  path = "/"
  description = "IAM policy for logging from a ecs task"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents",
        "logs:DescribeLogStreams"
      ],
      "Resource": "arn:aws:logs:*:*:*",
      "Effect": "Allow"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "ecs_service_logs" {
  role = "${aws_iam_role.ecs_service_role.name}"
  policy_arn = "${aws_iam_policy.ecs_service_logging.arn}"
}

#----- Service -----
module "this" {
  source  = "terraform-aws-modules/autoscaling/aws"
  version = "~> 4.4.0"

  name = local.resources_name

  update_default_version = true

  # Launch template
  lt_name = local.resources_name
  use_lt    = true
  create_lt = true

  image_id                  = data.aws_ami.amazon_linux_ecs.id
  instance_type             = "t2.micro"
  security_groups           = [module.vpc.default_security_group_id]  
  iam_instance_profile_arn  = module.ec2-profile.iam_instance_profile_arn
  user_data_base64 = base64encode(templatefile("${path.module}/templates/user-data.sh", {cluster_name = local.namespace}))
  target_group_arns = module.alb.target_group_arns

  # Auto scaling group
  vpc_zone_identifier       = module.vpc.public_subnets
  health_check_type         = "EC2"
  min_size                  = 0
  max_size                  = 1
  desired_capacity          = 1
  wait_for_capacity_timeout = 0
  
  tags = [
    {
      key                 = "Terraform"
      value               = "yes"
      propagate_at_launch = true
    },
    {
      key                 = "Owner"
      value               = local.owner
      propagate_at_launch = true
    },
    {
      key                 = "Cluster"
      value               = local.namespace
      propagate_at_launch = true
    },
    {
      key                 = "Namespace"
      value               = local.namespace
      propagate_at_launch = true
    },
    {
      key                 = "Environment"
      value               = local.stage
      propagate_at_launch = true
    },
    {
      key                 = "Usage"
      value               = local.usage
      propagate_at_launch = true
    },
  ]
}