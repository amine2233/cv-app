[![pipeline status](https://gitlab.com/Lonli-Lokli/cv-app/badges/develop/pipeline.svg)](https://gitlab.com/Lonli-Lokli/cv-app/-/commits/develop)  [![coverage](https://gitlab.com/Lonli-Lokli/cv-app/badges/develop/coverage.svg?job=test&key_text=API+Coverage&key_width=80)](https://gitlab.com/Lonli-Lokli/cv-app/-/commits/develop)  [![coverage](https://gitlab.com/Lonli-Lokli/cv-app/badges/develop/coverage.svg?job=test:jest&key_text=UI+Coverage&key_width=80)](https://gitlab.com/Lonli-Lokli/cv-app/-/commits/develop)

# Architecture
![architecture.svg](/infrastructure/architecture.svg)